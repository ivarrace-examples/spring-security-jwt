package com.ivarrace.springsecurityjwt;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

@SpringBootTest
class SpringSecurityJwtApplicationTests {

	@Test
	void contextLoads() {
		SpringSecurityJwtApplication.main(new String[0]);
	}

}
